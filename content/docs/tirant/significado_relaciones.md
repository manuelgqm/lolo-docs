---
title: "Significado de relaciones entre documentos"
description: ""
date: 2022-04-20T13:20:33+02:00
lastmod: 2022-04-20T13:20:33+02:00
draft: false
images: []
menu: 
  docs:
    parent: "tirant"
weight: 999
toc: true
---

## Contexto
Tenemos mucha documentación que está relacionada entre si por diversos motivos, actualmente utilizamos la información de esta relación con
motivos estadísticos pero se podría explotar de muchas otras maneras.

## Problema
Al mostrar análisis de la información estamos olvidando utilizar una parte muy importante de esta, ¿para qué está utilizando la información
en cada caso el usuario? ¿Que quiere conseguir con la información que obtenga? ¿Cómo podemos utilizar esta información de su propósito para
ayudarle en cada caso y realizar ese análisis desde la aplicación? ¿Tenemos toda la información disponible para poder ayudarle si sabemos
lo que realmente quiere? ¿Si no la tenemos, podemos calcularla, obtenerla o alimentarla de alguna manera?

## Propuesta
En el proyecto de mapeo de relaciones entre la documentación, además de la información mas evidente que se puede obtener de este modelo, es decir
número de enlaces de un determinado documento por cada nivel de profundidad, caminos de acceso a documentos, interrelación de conceptos incluidos,
se puede incluir información relacionada con el sentido de cada relación. Es decir, si usamos el modelo de la red neuronal del cerebro humano, 
la información, los recuerdos, tanto de datos como procedurales, se almacenan en el cerebro como relaciones de conexiones entre neuronas 
que tienen un determinado significado.
De igual forma podríamos dar un significado a una determinada interrelación de documentos, un ejemplo sencillo de esto sería sencillamente los
documentos que son necesarios para la realización de un determinado procedimiento en diferentes fase, o bien los documentos que se presentan
como alegaciones para un determinado caso y el fallo que ha producido este. 

## Beneficios
Dotando de esta significación de relaciones podríamos, por ejemplo, crear modelos predictivos del sentido del fallo de un caso, basándonos en la
documentación relaciona con la jurisprudencia existente y el sentido de cada fallo y la documentación que se presentó o cualquier otra información
relevante de que dispongamos en la bd. En este caso en concreto se podría mapear por triangulación según el sentido a favor o en contra, la parte
que se defiende, etc... Es decir, si fuera abogado y pudiera decir que tengo un caso de este tipo en el que defiendo a esta figura, y que quiero
conseguir determinado fallo, me sería muy útil que la aplicación me sugiriera determinada documentación, procedimientos, recomendaciones, etc..
de forma que a medida que voy trabajando en ello y adjuntando documentos a mi caso, ello me fuera prediciendo el porcentaje de probabilidad de éxito.
Esto se podría validad incluso con los propios datos de que ya se disponen, realizando simulaciones contra las mismas sentencias que ya de disponen.

## Por definir
Si se dispone de la información necesaria para realizar la trabilidad entre la generación y uso de la documentación, identificadores únicos de
tipo de documento que vengan en el propio documento y se puedan enlazar con otros, como el número de sentencia, de expediente, etc...
