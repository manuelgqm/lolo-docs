---
title: "Mejoras en resultados de buscadores"
description: ""
date: 2022-04-20T13:20:33+02:00
lastmod: 2022-04-20T13:20:33+02:00
draft: false
images: []
menu: 
  docs:
    parent: "tirant"
weight: 999
toc: true
---

## Contexto
Hay diversos parámetros que sirven para posicionar los resultados que produce una búsqueda de documentos en la base de datos.
En función de la configuración de estos parámetros, se obtiene una ordenación diferente.

Que estos ajustes se puedan probar en un entorno de pre.

Que si no se obtienen resultados en la busqueda con algunas opciones seleccionadas más restrictivas como “por proximidad”, “con la frase exacta”, si se obtienen resultados con opciones menos restrictivas, esto se informe al usuario y se proponga mostrar los resultados con el filtro menos restrictivo y también proponer el servicio de “se lo buscamos”.

Que los users puedan valorar los resultados de sus búsquedas con estrellas para que ello nos sirva para incluir esa información en la relevancia de los resultados.

Que se guarde la posición de los documentos que visitan en los resultados de la búsqueda, de forma que podamos usar esa información para saber cuando estamos ordenando con mayor efectividad según el uso.

## Problema
La evolución de la información y su naturaleza, así como el cambio en la demanda de su uso debida a factores del panorama de temas de mayora actualizad en el ámbito jurídico,
así como los diferentes intereses de los usuarios según su rol o caso de uso en particular hacen que encontrar un criterio de ordenación de los resultados óptimo, sea algo
para lo que hay que realizar muchas pruebas de ensayo y error, en base a los parámetos que lo configuran, por las personas especializadas en este ámbito,
que son las únicas con suficiente criterio para poder tomar la decisión de la configuración más óptima en cada momento. Ahora mismo hay dependencia del departamente de desarrollo
para realizar estos ajustes, que además necesitan de la participación del experto que valide los resultados en base a una colección de ejemplos lo suficientemente
significativa. Además de esto, no contamos con un histórico de estos cambios en la configuración, lo que dificulta revisiones de cambios posteriores. Tampoco contamos
con un entorno común donde realizar estas pruebas, para poder trabajar sobre ello con tranquilidad, antes de dar por buena la nueva configuración y subirla a producción.

## Propuesta
Independientemente de que la cantidad o utilidad de los valores de configuración sea la óptima, proporcionariamos al equipo de contenidos (o el que corresponda),
opciones de backoffice para que puedan configurar estos valores de forma que puedan refinar el posicionamiento de los resultados de la búsqueda. Sería necesario que
esto fuera configurable por pais/aplicación y por tipo buscador. Además habría que contar con un entorno sand-box para poder ver que resultados se obtienen según una determinada
configuración. 
Adicionalmente podríamos mantener un registro hitórico de cada configuración que se aplique, para que sirva como referencia y poder revertir cambios o realizar comparaciones.
Además si hacemos esto, podemos evaluar la efectividad del criterio si almacenamos la información de uso del usuario en base a los documentos que accede de la lista de resultados
para un determinado criterio, es decir, podríamos puntuar según el orden en el que hacen clic para acceder a los documentos y su posición en la lista, asociando esta información
al criterio de ordenación que esté en ese momento en vigor, poniendo por caso que la máxima puntuación sería en aquellos casos en los que el user accede al primer documento
de la lista y la peor puntuación, cuando se encuentra muy abajo en la lista. 

## Beneficios
El valor principal de la aplicación de acceso a información es justamente facilitar que el usuario encuentre lo que busca, esto es bastante complejo y esta mejora puede servir
para mejorar ese acceso y además como punto de partida para un sistema más ambicioso que sirva para la monitorización de la calidad de la información en términos de
accesibilidad y nos puede ayudar también ha detectar posibles carencias en la segmentación, automática o manual así como de sinonímia de términos y titulado o nomenclatura de
documentos.

## Por investigar
Si podría ser viable que los propios usuarios valoren los resultados de la búsqueda (a modo de estrellas) para que sirva de f.eedback personalizado que nos permita trazar perfiles de usuario
o bien presentar la información de forma más personalizada.
Valorar realizar búsquedas automáticas, cuando no se obtengan resultados con los filtros que tenga seleccionados, reduciendo estos filtros y abarcando un conjunto menos
restrictivo. Realizar esto en diferentes pasos, cada vez menos restrictivos hasta obtener algún resultado y en caso de no obtenerse, mostrar el servicio de "se lo buscamos".
