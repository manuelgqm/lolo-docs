---
title: "Glosario"
description: ""
lead: ""
date: 2021-05-13T18:36:01+02:00
lastmod: 2021-05-13T18:36:01+02:00
draft: true
images: []
menu: 
  docs:
    parent: "tirant"
eight: 999
toc: true
---

# This a header 1

## This a sample for the page
- List item 1
- List item 2

### This is a h3
1. one
2. two
3. tree

##### This is a h4
Awesome! `code here`

```ruby
p´"hello"
```
{{< highlight ruby >}}
class AlertsController < ApplicationController

  before_action :set_alert_class, :set_alert

  def show
      render json: @alert
  end

  def index
    response_with_objects(@alert_class.search(search_params),
                          @alert_class.search_count(search_params))
  end

  def create
    @alert ||= @alert_class.create_with_alert_user(alert_params)
    render json: @alert.as_json({ include: :alertable }), status: :ok
  end

  def destroy
    @alert.destroy
    render json: {result: 'ok'}
  end

  privat
{{< / highlight >}}
