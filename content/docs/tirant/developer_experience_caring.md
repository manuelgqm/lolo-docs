---
title: "Developer experience caring"
description: "Proyecto de cuidado y mejora de la experiencia del developer"
lead: ""
date: 2022-06-05T08:49:34+02:00
lastmod: 2022-06-05T08:49:34+02:00
draft: false
images: []
menu: 
  docs:
    parent: "tirant"
weight: 999
toc: true
---

# Problemática
El mundo de los desarrolladores es bastante particular. Este oficio nacido en sus orígenes bajo el paradigma
de la productividad de la época industrial, se parece hoy más que a un trabajo de peón en una cadena de producción,
al trabajo de un artista o un artesano. Esto es así porque conlleva unas altas dosis de creatividad y de capacidad
de resolución de problemas, actividades que sólo pueden darse en entornos de confianza, protegidos de excesos de
presión y estrés. Está demostrado científicamente, que a medida que aumentan los factores de estrés y presión,
se reduce la capacidad de creatividadad de las personas, así como su habilidad de pensamiento lateral, y asociativo
clave en el éxito de encontrar soluciones a los problemas en los que nos encontramos en esta profesión en nuestro día
a día.
Además de esto, el crecimiento de la complejidad de las aplicaciones de hoy en día conlleva que el código solo pueda
ser mantenido y extendido, por equipos cada vez más grandes, añadiendo de esta forma una dimensión social y de
organización del trabajo a la problemática del trabajo individual.
Como último aspecto del problema, existe también la alta rotación y la dificultad para atraer y mantener relaciones
profesionales, causadas por la alta demanda en el sector de profesionales con experiencia, que en la actualidad
está causando problemas de expeculación salarial por parte de empresas profesionales de contratación, que no dejan
de ser intermediarios como las inmobiliarias y lucha entre empresas por ofrecer las mejores condiciones de forma 
que puedan atraer a nuevos candidatos.

# Propuesta DXC
Developer experience caring, nace como una propuesta de solución que satisfaga a ambas partes, la empresa tecnológica
y los desarrolladores. Pretende buscar puntos en común de beneficio mútuo basada en la complicidad, la confianza,
el respeto a las competencias, la honestidad, evitando relaciones no sostenibles basadas en valores del paradigma
de la revolución industrial, tales como la lealtad, el autoritarismo jerárquico y la productividad a corto plazo,
por encima de la efectividad sostenible en el tiempo, basada en el valor que produce para un determinado actor 
de este ecosistema.

## Dimentsiones de DXC
El trabajo del desarrollo de software consiste básicamente en la abstración de problemas, flujos y tareas del
mundo real, en aplicacines informáticas que permiten realizar mejor estas actividades en algún aspecto, o que 
directamente posibilitan formas de hacer las cosas que de otra manera serían imposibles de realizar.
En este sentido, hay ciertos aspectos que son clave, la comunicación, la formación, la innovación, la 
experimentación, así como la escalabilidad y sostenibilidad, tanto de la actividad de negocio, como de los 
sistemas que lo soportan. Centrándonos en el aspecto técnico, esto conlleva implicitamente, código sostenible 
de alta fiabilidad, sistemas distribuidos escalables y bajo nivel de acoplamieto entre partes.

### Comunicación
Es el aspecto de mayor relevancia para un correcto desempeño de las funciones cuando intervienen varios actores,
y a medida que escalamos en tamaño o en complejidad, mayor relevancia tiene. En la comunicación hay aspectos
comunes, pero dependiendo del contexto donde aplica, existen determinadas particularidades. Por otra parte, 
la comunicación se realiza dentro de determinados círculos, pero también entre conjuntos de círculos. Por tanto,
para realizarla correctamente conlleva una serie de adaptaciones según el caso. De cualquier manera, siempre
será necesario encontrar el lenguaje más adecuado, así como ajustar el nivel de detalle, así como tener siempre
muy en cuenta el público hacia el que va dirigido nuestro mensaje.

#### Círculos de comunicación

##### Producto
Exterior: Clientes del producto (internos o externos), Dirección, socios y competencia.
Interior: Desarrolladores, diseñadores y sistemas.
Interno: Stakeholders, Product Managers, Product Owners, Product Designers y miembros del equipo de producto.
Medios: Feedback de Usuarios, Experiencia de Usuario, Mapa de producto, Naturaleza de producto, manuales de uso,
demos, vídeos de ayuda.

##### Desarrollo
Exterior: Comunidad de developers inter-empresas
Interior: Equipos de producto, otros equipos de desarrollo de la empresa, diseñadores, gente de sistemas.
Interno: Desarroladores del equipo. 
Medios: Código fuente, reuniones, documentación, README de proyectos.
