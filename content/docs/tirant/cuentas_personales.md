---
title: "Cuentas personales"
description: ""
date: 2022-04-20T13:20:33+02:00
lastmod: 2022-04-20T13:20:33+02:00
draft: false
images: []
menu: 
  docs:
    parent: "tirant"
weight: 999
toc: true
---

## Contexto
Actualmente, el login al repositorio de documentación judicial (tirant on line) puede realizarse de dos maneras: 
usando la cuenta de usuario, o bien a través de una cuenta de personalización. Según sea el caso, el usuario tiene
distintas posibilidades dentro de la aplicación. Por resumirlo, cuando usa el login de personalización (en adelante LP),
el usuario cuenta con una serie de opciones que permiten guardar datos personales, como búsquedas, anotaciones, o suscripciones
a cambios de documentos que con el login de usuario no tiene.

## Problema
Cuando el usuario no usa el LP, le proporcionamos un acceso que le restringe una serie de funcionalidades que van incluidas
dentro de la contratación del servicio, reduciendo así la calidad de su experiencia.
Es cierto que cuando intenta acceder a algún servicio de los mencionados, se le requiere el LP que después de completarlo
se le proporciona el servicio, pero esto, además de interrumpir su flujo de trabajo, le obliga a realizar un login adicional
lo cual, reduce igualmente la calidad de la experiencia de usuario.
Además de esto, al no disponer de la identificación personal desde el principio, hay una serie de ayudas de la interfaz que no
le podemos proporcionar, como recordarle selecciones de desplegables, filtros, etc... Es cierto que podríamos realizar el
almacenamiento de esta config en cookies, pero esto daría como resultado que estuviera vinculada al equipo y no a la cuenta.

## Propuesta
Lo que se propone es hacer siempre el login por personalización. De esta forma habría una cuenta de personalización principal, o
de administrador, vinculada al usuario y después las cuentas de personalización "normales" necesarias asociadas al mismo. Tal y 
como funciona en el sistema actual, la configuración de permisos seguiría almacenada en la cuenta principal. Para favorecer que
cada persona real use su própia cuenta personal en vez de una genérica, para los casos en los que no cuenten de correo electrónico
personal, pordríamos crearles una cuenta de correo.

## Beneficios
De forma directa, simplificamos el login al unificarlo y garantizamos siempres el acceso a toda la funcionalidad de que
disponga en su tipo de subscripción.
Para aquellos casos en los que se desee utilizar la cuenta principal como cuenta genérica para distintas personas, podríamos
habilitar una opción en la config del usuario que permitiera deshabilitar las opciones de personalización para la cuenta principal.
Para no cambiar el actual sistema de login y que este cambio sea transparente al usuario, mantendríamos la posibilidad de que
para el login de la cuenta principal se pudiera usar, su correo electrónico y el nombre de usuario.

## Por definir
Faltaría definir el sistema de administración de creación de nuevas cuentas de personalización asociadas a la cuenta principal.
Esto pordria hacerse desde el propio cliente desde la propia cuenta principal, concediendo el acceso a las CP que lo soliciten,
o incluso plantearse la posibilidad de gestionarlo para algunos casos o cuentas desde atención al cliente ATC.
