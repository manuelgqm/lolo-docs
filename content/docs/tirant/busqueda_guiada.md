---
title: "Búsqueda guiada"
description: ""
date: 2022-04-22T08:27:07+02:00
lastmod: 2022-04-22T08:27:07+02:00
draft: false
images: []
menu: 
  docs:
    parent: "tirant"
weight: 999
toc: true
---

## Contexto
El abogado necesita encontrar la información relacionada con su caso o cualquier otro tipo de gestión que esté realizando que requiera la consulta de
documentación legal. Necesita poder acceder a esta información de la forma más rápida posible y que aquello que encuentra le de confianza porque
se trata de documentación válida y fiable.
Ahora mismo para dar respuesta a esta necesidad, entre otras cosas le estamos proporcionando un buscador general y otros más específicos por tipo de
documento, suponiendo que el mismo ya puede realizar esta discriminación. En primer lugar puede introducir palabras para la búsqueda en una caja de
texto y además seleccionar unos filtros iniciales y opciones de búsqueda, para reducir el conjunto sobre el que se realizará la consulta.
Finalmente, tras ejecutar la acción de buscar, obtiene una lista de resultados priorizados, en base a un criterio compuesto por diferentes aspectos,
en los que se mezcla cálculos automáticos junto con modificadores de priorización manual. Además de esto, junto a los resultados se le proporciona una
serie de filtros adicionales, creados dinámicamente en el procesado de documentos por parte del indexador, adaptados a los resultados obtenidos.

## Problema
El principal problema del enfoque tradicional de esta solución reside en que obliga al usuario a realizar la traducción entre los aspectos que caracterizan
su caso de uso y las opciones que debe seleccionar para poder encontrar la información que sea relavante para el. Las soluciones actuales van enfocadas
principalmente en proporcionar más opciones de búsqueda, lo cual obliga al usuario a convertirse en mayor conocedor de la herramienta. Por otra parte
el nuevo enfoque del asistente Sofia hace justo el extremo opuesto, y aunque alivia la carga de trabajo del usuario, esto se hace a costa de incrementar
el coste de procesamiento de la información pudiendo causar al mismo tiempo, nichos de información inaccesible, por acoplamiento a la lógica de predicción,
además de posiblemente reducir el margen de acierto al depender completamente de la automatización de la segmentación semántica y conceptual.
Ambas soluciones parecen encontrarse en puntos extremos, el problema principal que se ve es que el usuario sabe bien cual es su caso de uso y esa información
no se está usando sino que se queda en su dominio.

## Propuesta
Esta propuesta de buscador guiado se parece en parte, a un punto intermedio entre los dos enfoques actuales. El buscador asistido, lo que pretende es resolver
este trabajo de traducción del caso de uso del usuario en los filtros o segmentaciones conceptuales y semánticas que después se utilicen para cualquier
sistema de búsqueda que lo vayan a consumir después. En el caso de de salir hacia un buscador tradicional, daría como resultado una serie de parámetros
de búsqueda predefinidos y en el de el asistente Sofía, sumistraría información segmentada que además de aligerar el peso del procesamiento, daria resultados
mas cercanos al caso de uso concreto. 
La solución consiste en proporcionar un asistente de búsqueda que realizaría una serie de preguntas dinámicas al usuario que se irán adaptando según las respuestas
anteriores que va dando, a modo de trazar una ruta hacia la documentación. Los resultados deberían ir mostrandose, a medida que el usuario va proporcionando respuestas
de forma que en el momento en el que encuentre la información que necesita para su caso de uso, pueda finalizarla e incluso guardar este camino realizado para
utilizar en un futuro a modo de plantilla. De esta forma le ahorramos hacerle más preguntas de lo necesario. Para cada respuesta iríamos seleccionando los filtros
que correpondieran de forma que el usuario pudiera cambiar alguno de ellos si no le satisface el resultado.
En cuanto a usabilidad, convendría que las respuestas pudieran darse utilizando únicamente el teclado, bien escribiendo o pudiendo seleccionar opciones
o movimientos del cursor.
Cada respuesta que se proporcione debería aparecer en una lista visible al usuario junto a los filtros que proporciona cada una de ellas. Debería poder
saltarse preguntas o modificarlar en cualquier momento.
En todo momento del proceso debería, poder ir atrás, empezar de nuevo, o terminar, bien porque ha encontrado lo que quería o porque se no lo ha encontrado y se
ha cansado de buscar. En ambos casos esta acción estaría informada y la registraríamos, para usar esta información como datos estadísticos para mejorar el
servicio, o incluso enviar una alerta al equipo de contenidos (o el que corresponda), para que pudiera actuar para analizar y corregir el problema. 
El el caso de presionar el boton de "desistir porque no encuentro lo que busco", se le ofrecerían usa serie de opciones que dispongamos para darle
solución, como mostrarle ayuda de como usar algún servicio y/o proponerle el servicio "se lo buscamos" si procediera. 
Añadiriamos también la opción "seguir en otro momento", que le guardaría lo que tiene hecho para poder continuar más adelante.
En el caso de "terminar porque he encontrado lo que busco" guardaríamos también las respuestas dadas por el usuario, para puntuar positivamente esa selección y
utilizarlo para darle mayor peso en búsquedas o bien para medir el nivel de satisfación/instisfación del usuario con el servicio.
Además de esto podríamos guardar la información de los documentos de los resultados que finalmente el usuario consulta y el orden en el que lo hace en relación
con la consulta que ha realizado de forma que pudieramos usar esta info de alguna manera, para mejorar igualmente la posición de los resultados. Es decir,
el primer documento que pulsa un usuario en los resultados con unos determinados criterios debería aparecer más arriba que los que consulta en segundo o tercer
orden, al menos para ese usuario.
Adicionalmente se podría empezar a evaluar es uso de esta información de forma agregada con conjuntos de usuarios para utilizar una especie de pseudo
inteligencia colectiva.
 
## Beneficios
Se han mencionado indirectamente en el anterior apartado. Principalmente ahorramos procesamiento mental al usario al realizar nosotros la traducción del
caso de uso en base a la información que ya conoce, en los filtros o segmentaciones de la información interna de que disponemos.
- Se mejorarían los resultados de las búsquedas.
- Se reduciría la carga del procesamiento del asistente Sofia, al proporcionarle algo más de segmentación en la información, además de presumiblemente
obtener resultados mas satisfactirorios al poder utilizar el contexto que nos proporciona.
- Utilizar la información de uso de los usuarios nos permitiria acercar más lo que proporcionamos a sus espectativas.
- Asistimos activamente, lo que proporciona una sensación de acompañamiento y asesoramiento. "Cuentame tu caso"

## Por definir
Hay mucha validación que realizar de cada una de las ideas, tanto en su aplicación sobre el proceso real, como su posibilidad en base a la información
de que disponemos y de las capacidades de nuestros sistemas.
Todo el sistema de gestión de arbol de decisiones en base a las rutas posibles según los distintos casos de usos.
Descubrimiento y mapeo de los diferentes casos de uso y sus dependencias con el rol de de cada usuario.
