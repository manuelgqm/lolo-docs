---
title: "Frontend testing"
date: 2022-05-04T15:40:30+02:00
draft: false
menu:
  docs:
    parent: "Frontend"

---

## Reinterpretación del concepto de unidad en el testing
Una unidad, no es un archivo, un método o una función, sino un caso de uso, independientemente de que
en este caso de uso intervengan varios componentes de software.
El objetivo de realizar test es obtener confianza de que nuestro código está realizando aquello que
esperamos que haga y principalmente nuestro sofware se compone de un conjunto de casos de uso que
pretenden satisfacer las características de una actividad que queremos gestionar desde una aplicación.
Los test de integración quedan entonces como aquellas pruebas que comprueban que la capa interna de los
casos de uso se comunican correctamente con capas externas, es decir que se cumplen los contratos, con
por ejemplo una api o sistema de gestión de persistencia externo.
Los tes de integración se separan del caso de uso principalmente para obtener mayor velocidad en su ejecución
y también para independizarnos de la comprobación de los contratos que en realidad son un detalle externo
al caso de uso.
Para terminar contaremos con tests de aceptación o end to end, en los que probaremos que toda la aplicación
funciona de principio a fin levantando todos los actores que son necesarios. Estos tests al se los más pesados
simplemente deberían testear el "happy path" de cada caso de uso, mientras que en los unitarios testearemos
todos los caminos posibles desde la perspectiva del usuario.

## Patrón de estructura para un test
Usamos los contextos (describe y/o context), para definir un escenário que comparten varios tests. Lo podemos
detectar cuando tengamos el mismo setup (Arrange o Given) para diferentes Acts.

## Tests Doubles
### Mocks
Podemos usar jest.mock('filename'), para que jest nos haga un mock automático de todos los métodos que se exportan
en el fichero que le pasamos por parámetro.
```
jest.mock('usersRepository.js')
```
Podemos también definir que valores devolverán las funciones o métodos mockeados, aplicando el método
.mockResolvedValue() sobre los que necesitemos
```
let usersRepo = new UsersRepository()
usersRepo.listAll.mockResolvedValue([{ user_id: 1, user_name: 'Paquito'}])
```
Usar .mockResolvedValueOnce() cuando lo que devolvemos solo sirve para la primera llamada

Para limpiar los mocks al final del test, podemos usar tres métodos distintos
- .mockClear(): Pone a cero las veces que se ha llamado ese mock
- .mockRestore(): Además de esto, limpia también el valor que devuelve el mock si le hemos puesto que resuelve un valor.
- .mockReset(): 

### Testeando con vuex
La idea es incluir la operación sobre los stores en el mismo tests unitario del caso de uso que estemos testeando, de esa
forma es más sencillo testear puesto que hay menos elementos y además cada parte del store se testea de forma contextualizada
en cada caso de uso. Para poder lograr esto, debemos extraer a un servicio de repositorio las llamadas a la API para
poder mockearlas en aquellos tests que hagan peticiones al store que se comuniquen con la BD.
Usando Testing Library, en todos los componentes que testemos que usen store, debemos pasarle el store como parámetro a la
función render() de TL, además para aquellos contextos en los que lo necesitemos, podremos pasarle el estado inicial de este
store al hacer el render del componente.
Para estos ca
[componente con repositorio y vuex testeado con Testing Library](https://github.com/CodelyTV/javascript-testing-frontend-course/blob/main/71-custom-renderers/src/test/00-Login.spec.js)

## Testing Library

### Uso correcto de selectores
- .getByRole => objetos de interacción como botones, links, etc. 
- .getByText => asercioes de contenido que se muestra en pantalla

### Componentes Lazy
Cuando se importen los componentes dinámicamente usando import envuelto en una función:
```
() => import('path/to/component')
```
al cargarse los componentes de forma asíncrona, es necesario envolver las aserciones en una función asíncrona waitFor
para garantizar que el componente está cargado en el tiempo de ejecución del test
```
await waitFor(expect(expected).toBe(actual))
```
### Comprobación de elementos no mostrados
En ocasiones tenemos elementos que se muestran de forma condicional y queremos testear que dado un determinado contexto
no se muestran, para esos casos es preciso utilizar los métodos que localizan elementos que vienen enunciados por query*
como por ejemplo queryByRol o queryByText. Esto es debido a que esos métodos devuelven null cuando no encuentran lo que
buscan, permitiéndonos realizar la aserción, mientras que si utilizamos otros métodos como los findBy*, no podremos
realizar la aserción, puesto que estos métodos devuelven una excepción al no encontrar el elemento, deteniendo la ejecución
decepción al no encontrar el elemento, deteniendo la ejecución
del test

### Recursos
- [Queries Testing Library](https://testing-library.com/docs/queries/about/#priority)
- [ARIA Roles](https://developer.mozilla.org/en-US/docs/Web/Accessibility/ARIA/ARIA_Techniques#roles)
- [Buenas prácticas](https://dev.to/danyparedes/buenas-practicas-con-angular-testing-library-59lp)
- [Sacar partido al testing](https://dev.to/danyparedes/obteniendo-el-mayor-valor-nuestros-tests-en-angular-50od)
- [Testing Playground](https://testing-playground.com)
