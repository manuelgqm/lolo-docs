---
title: "Code Smells"
description: ""
lead: ""
date: 2022-01-13T09:32:58+01:00
lastmod: 2022-01-13T09:32:58+01:00
draft: false
images: []
menu: 
  docs:
    parent: "Extreme programming"
weight: 999
toc: true
---

## Una lista de code smells comunes

### Magic numbers
Son todos aquellos números que aparecen en el código, bien dentro de una operación aritmética o en 
una operación condicional, que al no estar asignados a un nombre de variable, obligan a tener que
investigar para que significan, si vemos el código por primera vez, o bien hemos de volver a el
un tiempo más tarde.

Ej: 
bad:
`area = 5 * 3.1416`
well:
radius = 5
pi = 3.1416
area = redius * pi

### Mocks in tests
