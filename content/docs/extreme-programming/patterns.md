---
title: "Patterns and metaprogramming"
description: ""
lead: ""
date: 2022-01-17T09:42:09+01:00
lastmod: 2022-01-17T09:42:09+01:00
draft: false
images: []
menu: 
  docs:
    parent: "Extreme programming"
weight: 999
toc: true
---

## Singleton
Are clases or object that have only one instance. You use the methods of the class directly from it,
without creating new instances of it.
Some consider to avoid it because this could be used as an anti-pattern in ruby (ruby anti-pattern?)[https://www.rubyguides.com/2018/05/singleton-pattern-in-ruby/]

## Herencia basada en prototipos
Permite la creación de instancias de objetos nuevos, a partir de instancias de objetos ya existentes
de forma que además de heredar sus métodos, se hereda también el estado de estos. Esto puede resultar
útil cuando necesitas instancias a partir un objeto ya creado del cual tienes el mismo estado. Este
patrón además de esto, permite añadir métodos nuevos a la nueva instancia, sin tener que definir una
clase para ello. Este típo de propagación de herencia es la que se utiliza en javascript pero puede
emplearse en otros lenguajes.
Ejemplo en ruby
```
animal = Object.new

def animal.number_of_feet=(feet)
  @number_of_feet = feet
end

def animal.number_of_feet
  @number_of_feet
end

cat = animal.clone
cat.number_of_feet(4)

felix = cat.clone
p felix.number_of_feet # prints 4
```

## Método definido (defined method)
Es un patrón que permite la creación de métodos dinámicos

```
class Multiplier
  def self.create_multiplier(n)
    define_method("times_#{n}") do |val|
      val * n
    end
  end

  10.times { |i| create_multiplier(i)}
end

m = Multiplier.new

puts m.times_2(3) # prints 6
puts m.times_3(3) # prints 9
```

## Evitar los ifs
Hay incluso una (campaña)[https://francescocirillo.com/pages/anti-if-campaign] para evitarlos 
Se pueden usar diferentes técnicas según el caso.
### Creando una colección de valores seleccionados
Es útil cuando se debe realizar una operación solo sobre algunos elementos de la colección que cumplan
una determinada collección. Para conseguirlo, creamos un método para cada condición que necesitemos
que devuelva 'true' cuando esta se cumple.
Después hacemos un filtro sobre la colección completa, pasando como criterio el método de inspección.
Finalmente, iteramos sobre cada elemento de las subcolecciones filtradas y realizamos la acción correspondiente.
(coding without ifs)[https://thoughtbot.com/blog/coding-without-ifs]
def update_subscriptions
  subscriptions = Subscription.find :all
  expired = subscriptions.select {|each| each.expired?}
  expired.each {|each| each.renew!}
  active = subscriptions.reject {|each| each.expired?}
  active.each {|each| each.update!}
end

### Creando clases independientes para cada caso usando polimorfismo
Consiste en crear una clase padre que contiene el comportamiento y estado por defecto. Después creamos
las clases que hagan falta para cada caso que necesitemos. Finalmente invocamos la clase y el método,
utilizando la invocación dinámica sobre el nombre de la clase en el valor del string del caso.
(replace conditional with polymorfism)[https://www.moncefbelyamani.com/refactoring-technique-replace-conditional-with-polymorphism/]
class Shipping
  def initialize(weight)
    @weight = weight
  end

  private

  attr_reader :weight
end

class DE < Shipping
  def cost
    5 + weight * 0.6
  end
end

class GB < Shipping
  def cost
    10
  end
end

class US < Shipping
  def cost
    10
  end
end

class CN < Shipping
  def cost
    raise "We only sell digital products in China!"
  end
end

def shipping_cost(country:, weight: 0)
  Object.const_get(country.upcase).new(weight).cost
end
```
shipping = Object.new

def shipping.weight=(value)
  @weight = value
end

def shipping.weight
  @weight
end

def shipping.cost
  10
end

de = shipping.clone

def de.cost
  5 + weight * 0.6
end


```

## Cl√°usula de guarda

## Memoization

