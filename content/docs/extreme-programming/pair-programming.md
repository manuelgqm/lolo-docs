---
title: "Pair Programming"
description: "Definiciones y como implantar pair programming en un equipo de desarrollo"
lead: ""
date: 2022-01-10T09:16:35+01:00
lastmod: 2022-01-10T09:16:35+01:00
draft: false
images: []
menu: 
  docs:
    parent: "Extreme programming"
weight: 999
toc: true
---

# Definición
Consiste en una práctica originaria de Extreme Programming (XP), para trabajar por parejas sobre una tarea de programación, 
en la cual se establecen dos roles, el de navegante y el de conductor que son asumidos por cada miembro.
No se trata de ser más productivo sino, de reducir el waste (y acabar siendo más productivo).

## ¿Que NO es pair programming?
- Programar por parejas
- Trabajar más de dos personas en la misma tarea
- Escribir ambos el código al mismo tiempo
- Uno programa y el otro solo mira
- Uno programa y el otro mira el wasap o el facebook
- Trabajar de cualquier manera (indisciplinadamente)
- Cada uno hace una parte de la tarea
- Siempre escribe el mismo (el que más sabe)

## ¿Que SI es pair programming?
- Seguir la metodología (y adaptarla según el caso)
- Adoptar un rol y ceñirse a el
- Hacer rotaciones frecuentes
- Planificar
- Dialogar y consensuar
- Dar y recibir feedback contínuo
- Respetar a nuestro compañero y a nuestro equipo a través del código

## Preparar la sesión de pairing
- Entender bien la tarea
- Completar los huecos de conocimiento que puedan haber
- Definir la estrategia
- Crear el checklist del roadmap a completar

## El rol del piloto
- Codificar
- Comunicar contínuamente las intenciones
- Solicitar feedback sobre la marcha
- Pedir ayuda para consultas
- Pedir anotaciones
- Darse tiempo en silencio si es necesario, para pensar

## El rol del navegante
- Evitar romper el flow del piloto
  - No pensar o divagar en voz alta
  - Evitar la conversación no relacionados con la tarea
  - Evitar corregir errores de codificación (regla de los 5 seg.)
  - Tomar notas
  - Tareas emergentes
  - Comentarios sobre la tarea
  - Ideas que surjan
  - Concerns
- Favorecer el flow
  - Hacer las consultas necesarias (google, código, documentación...)
  - Visión de estructura o macro del código
  - Controlar la lista de tareas
  - Vigilar el scope de los commits
  - Proteger el foco, volviendo a lo que se está, cada vez que hay desviaciones

## Flavors de pair programming
- Senior-Senior
- Senior-Junior
- Junior-Junior

## Tips para el día a día para PP en remoto
- Decidir las hot-hours dedicadas
- Respetar a tu compañero
- Mantener el foco (no distraerse)
- Trabajar en pomodoros
- Feedback visual y auditivo contínuo (video en la pantalla de la cámara)
- Comprobar con frecuencia que se está en la misma página
- Hacer descansitos
- Hacer preguntas
- Compartir ambas pantallas
- Compartir código (Live Share)
- Si no hay acuerdo en la estrategia de implementación ceder para demostrar la hipótesis
- Celebrar las tareas cumplidas

## Desmintiendo tópicos
- Es algo muy moderno para gente guay (Jean Bartik 1945)
- Se es menos productivo (yo solo acabo antes)
- Solo sirve para hacer TDD
- Solo sirve para programar (Pairing)
- Es mejor hacerlo siempre y para todo

## Que ventajas ofrece
- Reduce el waste
  - Menor necesidad de documentación
  - Menos reuniones (sprint refinements, retros)
  - Menos review de código (una por sprint)
  - Menores desviaciones sobre el objetivo 
  - Menos errores
  - Reduce la complejidad de flujos de trabajo
  - Reduce la necesidad de sistemas de peer-feedback
  - Evita la necesidad de hacer merge reviews
- Poduce mejoras
  - Mejora la socialización del conocimiento
  - Mayor respeto por los convenios de código
  - Proporciona un feedback directo de tus compañeros
  - Mejora la cohesión de los equipos
  - Mejora la calidad del código
  - Mejora la arquitectura del código
  - Favorece formas de trabajo estructuradas
  - Favorece el debate
  - Mejora social en el trabajo (especialmente necesario en remoto)

## Cuando y cómo aplicarlo
- Aporta mayor valor cuanto mayor es el grado de incertidumbre
- Evitarlo en tareas triviales
- 

## ¿Porqué fracasa pair programming?
- Dificultades individuales
  - Resistencia al cambio
  - Falsa sensación de ser menos productivo
  - Falta de autodisciplina en el orden y la atención
  - Apego a los ritmos y maneras propias
  - Conflicto de infravaloración de uno mismo
  - Dificultades para mantener el foco de atención
- Falta de rigor en el método
  - Falta de descansos
  - No se siguen correctamente el cometido de los roles
  - No saber cuando parar y/o dividir el trabajo
- Dificultades interpersonales
  - Dificultad para adaptarse a los demás (ritmos, conocimiento)
  - Miedo a exponer la própia ignorancia
  - Favoritismos por determinadas personas (parejitas)
  - Falta de confianza para decirle a tu pareja que te está troleando
  - Falta de acuerdo y discusiones sobre como abordar una tarea (competitividad)

## Huecos de conocimiento
- Round robin

## Bibliografía
[Martin Fowler](https://martinfowler.com/articles/on-pair-programming.html)
