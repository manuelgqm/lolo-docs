---
title: "Tasks"
description: ""
lead: ""
date: 2022-03-14T09:01:17+01:00
lastmod: 2022-03-14T09:01:17+01:00
draft: true
images: []
menu: 
  docs:
    parent: "methodology"
weight: 999
toc: true
---

## TODO
* Create a template for including the frozen_string literal on vscode
* Print current branch in iterm tab title
* Create my own dotfiles personal gitlab repo
* Create a fast up project system
  - It has to check if local dependencies are up, like mongo, mysql or redis
  - It should have config options for branches, terminal tabs creation, etc.
  - Dockerize needs of local services like mysql, mongo, redis, etc.
* Create local development environment
  - Retrieving some data from production, develop with a script.
  - Using a separate DB from testing for development
* Investigate Vim normal mode in terminal, browser, everywhere XD
* Disable arrows in vim
* Reconect VPN automatically when it's down

## DOING

## DONE
* Mege develop in bddv-693
* Change automate.io message for events date update set "Fulanito ha
  programado el siguiente evento en la agenda del equipo.

## Task types

### Administrative
- Move information to LoloDoc
- Move to digital ideas in paper
