---
title: "Daily Planning"
description: ""
lead: ""
date: 2022-03-14T08:26:34+01:00
lastmod: 2022-03-14T08:26:34+01:00
draft: false
images: []
menu: 
  docs:
    parent: "Metodologías"
weight: 999
toc: true
---

## Blocks (from monday to thursday)
* 08:00 - 09:00: Admin tasks (plan the day)
* 09:00 - 10:30: Coding
* 10:30 - 11:00: Morning brake
* 11:00 - 11:30: Daily
* 11:30 - 13:30: Coding
* 13:30 - 14:00: Admin tasks
* 14:00 - 15:00: Lunch Break
* 15:00 - 16:00: Learning and Training
* 16:00 - 17:00: Coding or Product
* 17:00 - 17:30: Admin tasks

## Blocks (fridays)
* 08:00 - 09:00: Admin tasks (plan the day)
* 09:00 - 10:30: Code
* 10:30 - 11:00: Morning brake
* 11:00 - 11:30: Daily
* 11:30 - 12:30: Code or Product documentation
* 12:30 - 13:30: Review team's week code
* 13:30 - 14:00: Close the week (review done and plan next week)

## Task types
### Admin tasks
1. Harvest notes (create tasks)
2. Sechedule
3. Do meetings
4. Send messages

### Coding
1. Code reviews
2. Merge code
3. Work on active task

### Product
1. Product mapping
2. US refinement
