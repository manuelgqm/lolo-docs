---
title: "Notes"
description: "This are miscelaneous notes waiting to find their site"
date: 2021-05-14T10:58:12+02:00
lastmod: 2021-05-14T10:58:12+02:00
draft: true
menu:
  docs:
    parent: "Metodologías"
---

Bueno, por fin lo he sacado.
Al guardar un documento con el tipo Epigrafe BV latam ejecuta las validaciones y ahí es donde se rellenan todos esos errores que muestra
en la clase Latam::Documento hay y método que se llama validate_by_type que al final llamará a doctrina_validation en el concern Doctrina
ahí hay un condicional en la línea 31 que mete los errores que tocan
o sea, habría que hacer algo similar para el campo ese nuevo para españa y supongo que funcionará solo
estoy investigando  de todos modos cómo hace para guardar esa información luego porque ya los campos se muestran siempre, y lo que hace en ese condicional en teoría no persiste nada en DB
Aaaarg, pues no, eso sólo muestra los errores, aún no se cómo hace lo de los campos, voy a seguir xD

Vale, creo que hay que añadir unas entradas al mongo para que eso funcione
Se ve que para ver si un campo es requerido o no se miran dos campos: el tipoid y el is_required
Si ejecutas esto en el mongo db.getCollection('fields').find({name: "numeroepigrafe"})
verás que tiene dos entradas
/* 1 */
{
    "_id" : ObjectId("5a8ed21a7dcb6d0d02bc2d2d"),
    "orden" : 10,
    "_type" : "Latam::Edicion::DocumentoDescriptorField",
    "print_section" : "title",
    "name" : "numeroepigrafe",
    "tipoid" : 1102,
    "value_type" : "string",
    "is_visible_en_web" : true,
    "is_indexed" : true,
    "is_listado" : true,
    "is_required" : true,
    "in_descriptores" : false,
    "is_multi_idioma" : false,
    "is_array" : false,
    "is_docid" : false,
    "is_epigrafe_bv" : false
}

/* 2 */
{
    "_id" : ObjectId("5a8ed21a7dcb6d0d02bc2d30"),
    "orden" : 10,
    "_type" : "Latam::Edicion::DocumentoDescriptorField",
    "print_section" : "title",
    "name" : "numeroepigrafe",
    "tipoid" : 1103,
    "value_type" : "string",
    "is_visible_en_web" : true,
    "is_indexed" : true,
    "is_listado" : true,
    "is_required" : true,
    "in_descriptores" : false,
    "is_multi_idioma" : false,
    "is_array" : false,
    "is_docid" : false,
    "is_epigrafe_bv" : false
}
Entonces, al campo este de GPS no se qué tipoid le corresponderá, pero según el que sea habrá que asignarlo aquí a las entradas que habrá que crear por cada uno de los campos requeridos cuando se selecciona el GPS
Por entrar en más detalle, esto se hace en el GET del toldocslat, en DocumentController#edit la función descriptores_fields hace una petición a apitol a Edicion::FieldsController#index que es el que lee estas entradas del mongo
O sea, lo que habíais añadido antes al switch case ese estaría bien y además habría que crear las entradas estas y también hacer lo que te comenté arriba de doctrina_validation para mostrar los errores igual que en el caso de epigrafe bv
Pero puff, menuda complicación para ver cómo funciona todo :sweat_smile:


https://guides.rubyonrails.org/debugging_rails_applications.html#verbose-query-logs

=> [#<TolDocument::Edicion::Field backoffice_user_id: nil, fieldname: "descriptor", fieldtype: "documento", filtro_clave: nil, id: "60a63dd956ba7d2f1d514aba", in_descriptores: false, is_array: false, is_docid: false, is_epigrafe_bv: false, is_indexed: false, is_listado: true, is_multi_idioma: false, is_required: false, is_visible_en_web: true, model_associated: nil, name: "fechadoctrina", orden: 50, print_section: "header", table_field: nil, table_name: nil, tipoid: 1, updates_count: 0, value_type: "string">,
 #<TolDocument::Edicion::Field backoffice_user_id: nil, fieldname: "descriptor", fieldtype: "documento", filtro_clave: nil, id: "60a63dd956ba7d2f1d514ab7", in_descriptores: false, is_array: false, is_docid: false, is_epigrafe_bv: false, is_indexed: false, is_listado: false, is_multi_idioma: false, is_required: true, is_visible_en_web: false, model_associated: "tipo_doctrina", name: "tipo", orden: 1000, print_section: "header", table_field: nil, table_name: nil, tipoid: 1, updates_count: 0, value_type: "integer">,
 #<TolDocument::Edicion::Field backoffice_user_id: nil, fieldname: "descriptor", fieldtype: "documento", filtro_clave: nil, id: "60a63dd956ba7d2f1d514ab8", in_descriptores: false, is_array: false, is_docid: false, is_epigrafe_bv: false, is_indexed: true, is_listado: false, is_multi_idioma: false, is_required: true, is_visible_en_web: false, model_associated: "libro_doctrina", name: "librodoctrina", orden: 1000, print_section: "header", table_field: "titulo", table_name: "libro_doctrina", tipoid: 1, updates_count: 0, value_type: "integer">,
 #<TolDocument::Edicion::Field backoffice_user_id: nil, fieldname: "descriptor", fieldtype: "documento", filtro_clave: nil, id: "60a63dd956ba7d2f1d514ab9", in_descriptores: false, is_array: true, is_docid: false, is_epigrafe_bv: false, is_indexed: true, is_listado: false, is_multi_idioma: false, is_required: false, is_visible_en_web: true, model_associated: "autor_doctrina", name: "autores", orden: 1000, print_section: "header", table_field: "autor", table_name: "despdautor", tipoid: 1, updates_count: 0, value_type: "integer">]

 => [#<TolDocument::Edicion::Field backoffice_user_id: nil, fieldname: "descriptor", fieldtype: "documento", filtro_clave: nil, id: "5a8ed21a7dcb6d0d02bc2d30", in_descriptores: false, is_array: false, is_docid: false, is_epigrafe_bv: false, is_indexed: true, is_listado: true, is_multi_idioma: false, is_required: true, is_visible_en_web: true, model_associated: nil, name: "numeroepigrafe", orden: 10, print_section: "title", table_field: nil, table_name: nil, tipoid: 1103, updates_count: 0, value_type: "string">,
 #<TolDocument::Edicion::Field backoffice_user_id: nil, fieldname: "descriptor", fieldtype: "documento", filtro_clave: nil, id: "5a8ed21a7dcb6d0d02bc2d31", in_descriptores: false, is_array: false, is_docid: false, is_epigrafe_bv: false, is_indexed: true, is_listado: true, is_multi_idioma: false, is_required: true, is_visible_en_web: true, model_associated: nil, name: "tituloepigrafe", orden: 20, print_section: "title", table_field: nil, table_name: nil, tipoid: 1103, updates_count: 0, value_type: "string">,
 #<TolDocument::Edicion::Field backoffice_user_id: nil, fieldname: "descriptor", fieldtype: "documento", filtro_clave: nil, id: "5ed4e144e8ce7a10e3263990", in_descriptores: false, is_array: false, is_docid: false, is_epigrafe_bv: false, is_indexed: false, is_listado: true, is_multi_idioma: false, is_required: false, is_visible_en_web: true, model_associated: nil, name: "fechadoctrina", orden: 50, print_section: "header", table_field: nil, table_name: nil, tipoid: 1, updates_count: 0, value_type: "string">,
 #<TolDocument::Edicion::Field backoffice_user_id: nil, fieldname: "descriptor", fieldtype: "documento", filtro_clave: nil, id: "5a8ed21a7dcb6d0d02bc2d29", in_descriptores: false, is_array: false, is_docid: false, is_epigrafe_bv: false, is_indexed: false, is_listado: false, is_multi_idioma: false, is_required: true, is_visible_en_web: false, model_associated: "tipo_doctrina", name: "tipo", orden: 1000, print_section: "header", table_field: nil, table_name: nil, tipoid: 1, updates_count: 0, value_type: "integer">,
 #<TolDocument::Edicion::Field backoffice_user_id: nil, fieldname: "descriptor", fieldtype: "documento", filtro_clave: nil, id: "5a8ed21a7dcb6d0d02bc2d2a", in_descriptores: false, is_array: false, is_docid: false, is_epigrafe_bv: false, is_indexed: true, is_listado: false, is_multi_idioma: false, is_required: true, is_visible_en_web: false, model_associated: "libro_doctrina", name: "librodoctrina", orden: 1000, print_section: "header", table_field: "titulo", table_name: "libro_doctrina", tipoid: 1, updates_count: 0, value_type: "integer">,
 #<TolDocument::Edicion::Field backoffice_user_id: nil, fieldname: "descriptor", fieldtype: "documento", filtro_clave: nil, id: "5a8ed21a7dcb6d0d02bc2d2b", in_descriptores: false, is_array: true, is_docid: false, is_epigrafe_bv: false, is_indexed: true, is_listado: false, is_multi_idioma: false, is_required: false, is_visible_en_web: true, model_associated: "autor_doctrina", name: "autores", orden: 1000, print_section: "header", table_field: "autor", table_name: "despdautor", tipoid: 1, updates_count: 0, value_type: "integer">,
 #<TolDocument::Edicion::Field backoffice_user_id: nil, fieldname: "descriptor", fieldtype: "documento", filtro_clave: nil, id: "5a8ed21a7dcb6d0d02bc2d32", in_descriptores: false, is_array: false, is_docid: false, is_epigrafe_bv: false, is_indexed: false, is_listado: false, is_multi_idioma: false, is_required: true, is_visible_en_web: false, model_associated: nil, name: "orden", orden: 1000, print_section: "header", table_field: nil, table_name: nil, tipoid: 1103, updates_count: 0, value_type: "integer">,
 #<TolDocument::Edicion::Field backoffice_user_id: nil, fieldname: "descriptor", fieldtype: "documento", filtro_clave: nil, id: "5a8ed21a7dcb6d0d02bc2d33", in_descriptores: false, is_array: false, is_docid: false, is_epigrafe_bv: true, is_indexed: false, is_listado: false, is_multi_idioma: false, is_required: true, is_visible_en_web: false, model_associated: nil, name: "idepigrafe", orden: 1000, print_section: "header", table_field: nil, table_name: nil, tipoid: 1103, updates_count: 0, value_type: "string">]


apitol development.rb   
Mongoid.logger.level = Mongo::Logger.logger.level = Logger::INFO

toldocslat development.rb 
config.assets.debug = false
config.assets.quiet = false

# Entorno de desarrollo local para Tirantcloud en tolweb
- Para que se puedan leer los archivos js desde la carpeta del equipo local, descomentar o añadir en 
el fichero `application_controller.rb` la línea: `content_security_policy false`
- En el fichero `config/setting/development.yml` descomentar las líneas de los includes de js, para 
usar los archivos locales de las aplicaciones front levantadas en local, tirantcloud y sofia.
- Al realizar modificaciones en el front de sofia (sofia-js), debemos copiar el archivo transpilado
en tirant-cloud-js, para tener los cambios aquí.
`yarn build && cp dist/sofia-1.1.11.js ../tirant-cloud-js/lib/sofia.js`

# Tirantcloud-service
- Para ver correctamente los errores en el log, en el archivo files_controler.rb comentar la línea con:
> around_action :rescue_errors

## Minio
- En la api de tirantcloud (cloud service), se puede acceder a un emulador de gestión de archivos en
la nube, llamado minio, para poder ver los archivos que se puden consultar desde la la aplicación.
Se puede consultar en la ruta http://localhost:9002
- Para que esté accesible, hay que añadir a la línea del docker-compose.yml del servicio de minio
lo siguiente `server /data{1...4} --console-address ":9002"``


# Para revisar
- [Stimulus reflex](https://docs.stimulusreflex.com) es un framework que funciona con Rails para hacer la parte frontend comunicando por
websockets con el server. Está basado en arquitectura de compomentes como los típicos frameworks de front.
- [Svelte frontend framework](https://svelte.dev)
- [Deno a node replacement](https://deno.land)
- [Podman the new generation Docker](https://podman.io)
