---
title: "Vim"
description: "Vim help and tricks"
lead: ""
date: 2021-05-17T12:24:45+02:00
lastmod: 2021-05-17T12:24:45+02:00
draft: true
images: []
menu:
  docs:
    parent: "tools"
weight: 999
toc: true
---

## Docucumentation
- [Neovim Plugins](https://stsewd.dev/es/posts/neovim-plugins/)
- [Your problem with Vim is that you don't grok vi](https://gist.github.com/nifl/1178878)

## Windows nNavigation
- C-[hjkl] -> Go to window in position
- [n]g -> Go to to talb

## Text navigation
- w -> First letter of next word
- e -> Last letter of current word
- b -> First letter of current word
- W, B -> Includes dots as a part of the word
- 0 -> Beginning of curren line
- $ -> End of current line
- gg -> Beginning of file
- G -> End of file
- [n]G -> Line number
- % -> Closure character

## Text edition
- u -> undo last action
- U -> undo changes on current line
- C-r -> redo last action
- x -> removes current character
- r -> replaces current character
- R -> replaces text as you type
- ce -> changes until end of word
- cc -> changes whole line
- c[n][m] -> changes n times to m motion
- o, O -> inserts a line below, or avobe
- J -> joins current line with below
- y[m] -> yanks on motion
- v -> starts visual mode selection

## Text searching and replacing
- /[string] -> Find forward
- ?[string] -> Find backwards
- C-o -> Previous position
- C-l -> Next position
- :s/old/new/[g][c] -> replaces string [g] globally [c] prompting
- :#,#s/old/new -> replaces string between line numbers
- C-n -> current word match

## Window management
- C-ws C-wv -> Split windows horizontally and vertically

## File management
- v[do_selection]:w [filename] -> creates a new file with selected text
- :![shell_command] -> executes a shell command



## Knowledge gaps
- How do I delete a text without overwriting the current buffer?
- How to use multicursors


