---
title: "Git"
description: ""
lead: ""
date: 2021-05-25T15:23:43+02:00
lastmod: 2021-05-25T15:23:43+02:00
draft: true
images: []
menu: 
  docs:
    parent: "tools"
weight: 999
toc: true
---

## Pull remote branch
git checkout -b <new_branch_name> <remote_name>/<remote_branch_name>
